from django.urls import path

from . import views
app_name = 'form'

urlpatterns =[
    path('homepage/', views.homepage, name="homepage"),
    path('create/', views.create, name='create'),
    path('index/', views.index, name='index'),
    path('detail/P<id_post>/', views.detail, name='detail'),
    path('delete/P<id_post>/', views.delete, name='delete'),
]