from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
# Create your views here.
from .forms import PostForm
from .models import PostModel

def homepage(request):
    return render(request, 'form/homepage.html')

def index(request):
    posts = PostModel.objects.all()
    context = {
        'page_title':"Jadwal",
        'posts':posts,
    }
    return render(request, 'form/index.html', context)

def create(request):
    post_form = PostForm(request.POST or None)

    if post_form.is_valid():
        if request.method == 'POST':
            PostModel.objects.create(
                mata_kuliah = post_form.cleaned_data.get('mata_kuliah'),
                dosen = post_form.cleaned_data.get('dosen'),
                jumlah_sks = post_form.cleaned_data.get('jumlah_sks'),
                deskripsi_matkul = post_form.cleaned_data.get('deskripsi_matkul'),
                semester = post_form.cleaned_data.get('semester'),
                ruang_kelas = post_form.cleaned_data.get('ruang_kelas'),
            )
            return HttpResponseRedirect("/form/index/")

    context = {
        'page_title':'Add Courses',
        'post_form':post_form
    }
    return render(request, 'form/create.html', context)

def detail(request, id_post):
    matkul = PostModel.objects.filter(id=id_post).get(id=id_post)
    response = {
        'post':matkul
    }
    return render(request, 'form/detail.html', response)

def delete(request, id_post):
    try:
        delete_courses = PostModel.objects.filter(id = id_post)
        delete_courses.delete()
        return redirect('form:index')
    except:
        return redirect('form:index')
