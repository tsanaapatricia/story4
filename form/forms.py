from django import forms

class PostForm(forms.Form):
    mata_kuliah =forms.CharField(
        label = "Nama Mata Kuliah",
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )
    dosen         =forms.CharField(
        label = "Dosen",
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )
    jumlah_sks   =forms.CharField(
        label = "Jumlah SKS",
        max_length=50,
        widget=forms.NumberInput(
            attrs={
            'class':'form-control',
            'min':'1',
            'max':'6',
            }
        )
    )
    deskripsi_matkul    =forms.CharField(
        label = "Deskripsi mata kuliah",
        max_length=50,
        widget = forms.Textarea(
            attrs={
                'class':'form-control',
            }
        )
    )
    semester         =forms.CharField(
        label = "Semester",
        max_length=50,
        widget=forms.NumberInput(
            attrs={
            'class':'form-control',
            'min':'1',
            'max':'8',
            }
        )
    )
    ruang_kelas      =forms.CharField(
        label = "Ruang kelas",
        max_length=50,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )
    