from django.db import models

# Create your models here.
class PostModel(models.Model):
    mata_kuliah   =models.CharField(max_length=50)
    dosen         =models.CharField(max_length=50)
    jumlah_sks   =models.IntegerField()
    deskripsi_matkul    =models.CharField(max_length=100)
    semester         =models.IntegerField()
    ruang_kelas      =models.CharField(max_length=50)

    def __str__(self):
        return self.mata_kuliah