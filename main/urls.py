from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('edu/', views.edu, name='edu'),
    path('profil/', views.profil, name='profil'),
  
]
