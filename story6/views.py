from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import FormKegiatan, FormOrang
from .models import Kegiatan, Orang
# Create your views here.

def index(request):
    return render(request, 'story6/index.html')

def listnya(request):
    kegiatan = Kegiatan.objects.all()
    orang = Orang.objects.all()

    return render(request, 'story6/list.html', {'kegiatan': kegiatan, 'orang': orang})

def add(request):
    form_kegiatan = FormKegiatan(request.POST or None)

    if form_kegiatan.is_valid():
        if request.method == 'POST':
            Kegiatan.objects.create(
                n_kegiatan = form_kegiatan.cleaned_data.get('n_kegiatan'),
                desk_kegiatan = form_kegiatan.cleaned_data.get('desk_kegiatan'),
            )
            return HttpResponseRedirect("/story6/list/")

    context = {
        'page_title':'Add Kegiatan',
        'form_kegiatan':form_kegiatan
    }
    return render(request, 'story6/add.html', context)

def regist(request, id_post):
    form_orang = FormOrang(request.POST or None)

    if form_orang.is_valid():
        if request.method == 'POST':
            data_orang = form_orang.cleaned_data
            orang_regist = Orang()
            orang_regist.nama_orang = data_orang['nama_orang']
            orang_regist.kegiatan = Kegiatan.objects.get(id=id_post)
            orang_regist.save()
            return redirect('/story6/list')
    
    context = {
        'page_title':'Register',
        'form_orang':form_orang
    }
    return render(request, 'story6/regist.html', context)
        
def delete(request, id_post):
    delete_user = Orang.objects.filter(id = id_post)
    delete_user.delete()
    return redirect('story6:list')