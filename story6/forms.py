from django import forms

class FormKegiatan(forms.Form):
    n_kegiatan=forms.CharField(
        label = "Nama Kegiatan",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )
    desk_kegiatan=forms.CharField(
        label = "Deskripsi Kegiatan",
        max_length=100,
        widget=forms.Textarea(
            attrs={
                'class':'form-control',
            }
        )
    )

class FormOrang(forms.Form):
    nama_orang   =forms.CharField(
        label = "Nama Orang",
        max_length=100,
        widget=forms.TextInput(
            attrs={
            'class':'form-control',
            }
        )
    )
    