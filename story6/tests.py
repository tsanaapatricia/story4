from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .models import Kegiatan, Orang
from .views import index, add, listnya, delete, regist
from .forms import FormKegiatan, FormOrang
from .apps import Story6Config
# Create your tests here.

class ModelsTest(TestCase):  
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(n_kegiatan="makan", desk_kegiatan="makan tahu")
        self.orang = Orang.objects.create(nama_orang="Tsanaa")

    def test_instance_created(self):
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(Orang.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.kegiatan), "makan")
        self.assertEqual(str(self.orang), "Tsanaa")

class FormTest(TestCase):
    def test_form_is_valid(self):
        form_kegiatan = FormKegiatan(data={
            "n_kegiatan":"makan",
            "desk_kegiatan":"makan tahu"
        })
        self.assertTrue(form_kegiatan.is_valid())
        form_orang = FormOrang(data={
            "nama_orang":"Tsanaa",
        })
        self.assertTrue(form_orang.is_valid())

    def test_form_invalid(self):
        form_kegiatan = FormKegiatan(data={})
        self.assertFalse(form_kegiatan.is_valid())
        form_orang = FormOrang(data={})
        self.assertFalse(form_orang.is_valid())


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.kegiatan = Kegiatan.objects.create(
            n_kegiatan="makan", desk_kegiatan="makan tahu")
        self.orang = Orang.objects.create(
            nama_orang="Tsanaa", kegiatan=Kegiatan.objects.get(n_kegiatan="makan"))
        self.index = reverse("story6:index")
        self.add = reverse("story6:add")
        self.listnya = reverse("story6:list")
        self.regist = reverse("story6:regist", args=[self.kegiatan.pk])
        self.delete = reverse("story6:delete", args=[self.orang.pk])

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/index.html')

    def test_GET_list(self):
        response = self.client.get(self.listnya)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/list.html')

    def test_GET_add(self):
        response = self.client.get(self.add)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/add.html')

    def test_POST_add(self):
        response = self.client.post(self.add,{'n_kegiatan': 'makan', 'desk_kegiatan': "makan tahu"}, follow = True)
        self.assertEqual(response.status_code, 200)

    def test_POST_add_invalid(self):
        response = self.client.post(self.add,{'n_kegiatan': '', 'desk_kegiatan': ""}, follow = True)
        self.assertTemplateUsed(response, 'story6/add.html')

    def test_GET_delete(self):
        kegiatan = Kegiatan(n_kegiatan="makan", desk_kegiatan="makan tahu")
        kegiatan.save()
        orang = Orang(nama_orang="Tsanaa",
                      kegiatan=Kegiatan.objects.get(pk=1))
        orang.save()
        response = self.client.get(reverse('story6:delete', args=[orang.pk]))
        self.assertEqual(Orang.objects.count(), 1)
        self.assertEqual(response.status_code, 302)


class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_urls_index_available(self):
        response = Client().get("/story6/index")
        self.assertEquals(response.status_code, 301)

    def test_urls_regist_available(self):
        response = Client().get('/story6/regist')
        self.assertEquals(response.status_code, 404)

    def test_urls_add_available(self):
        response = Client().get("/story6/add")
        self.assertEquals(response.status_code, 301)

    def test_urls_list_available(self):
        response = Client().get('/story6/list')
        self.assertEquals(response.status_code, 301)

class HTMLTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_index(self):
        response = Client().get('/story6/index/')
        self.assertTemplateUsed(response, 'story6/index.html')

    def test_add(self):
        response = Client().get('/story6/add/')
        self.assertTemplateUsed(response, 'story6/add.html')

    def test_list(self):
        response = Client().get('/story6/list/')
        self.assertTemplateUsed(response, 'story6/list.html')

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(Story6Config.name, 'story6')

class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(n_kegiatan="makan", desk_kegiatan="makan tahu")
        kegiatan.save()

    def test_regist_POST(self):
        response = Client().post('/story6/regist/1/',
            data={'nama_orang': 'Tsanaa'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/story6/regist/1/')
        self.assertTemplateUsed(response, 'story6/regist.html')
        self.assertEqual(response.status_code, 200)

    def test_regist_POST_invalid(self):
        response = Client().post('/story6/regist/1/',
            data={'nama_orang': ''})
        self.assertTemplateUsed(response, 'story6/regist.html')
        self.assertEqual(response.status_code, 200)
