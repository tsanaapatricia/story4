from django.urls import path

from . import views
app_name = 'story6'

urlpatterns =[
    path('index/', views.index, name="index"),
    path('add/', views.add, name='add'),
    path('list/', views.listnya, name='list'),
    path('regist/<int:id_post>/', views.regist, name='regist'),
    path('delete/<int:id_post>/', views.delete, name='delete'),
]