from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    n_kegiatan = models.CharField(max_length=100)
    desk_kegiatan = models.CharField(max_length=100)
    
    def __str__(self):
        return self.n_kegiatan

class Orang(models.Model):
    nama_orang = models.CharField(max_length=100)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.nama_orang
